package com.projects.mymultifunctionalapp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.projects.mymultifunctionalapp.R;

public class GoogleActivity extends Activity {

    private WebView google;
    private Button btnBack;
    private ProgressBar prgLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google);

        getLayoutObjects();

        google = findViewById(R.id.google);
        google.setWebViewClient(new MyWebViewClient());
        google.loadUrl("https://www.google.com");

        WebSettings webSettings = google.getSettings();
        webSettings.setJavaScriptEnabled(true);
        google.getSettings().setBuiltInZoomControls(true);

        btnBack.setOnClickListener((View v) -> {

            GoogleActivity.this.finish();
        });
    }

    @Override
    public void onBackPressed() {
        if (google.isFocused() && google.canGoBack()) {
            google.goBack();
        } else {

        }
    }

    private void getLayoutObjects() {
        btnBack = findViewById(R.id.btnBack);
        prgLoading = findViewById(R.id.prgLoading);
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            prgLoading.setVisibility(View.GONE);
        }
    }

}
