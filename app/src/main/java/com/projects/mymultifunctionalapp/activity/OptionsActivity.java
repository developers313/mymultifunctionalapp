package com.projects.mymultifunctionalapp.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.projects.mymultifunctionalapp.R;
import com.projects.mymultifunctionalapp.fragment.ContactFragment;
import com.projects.mymultifunctionalapp.fragment.LanguageFragment;
import com.projects.mymultifunctionalapp.fragment.TodoListFragment;

public class OptionsActivity extends AppCompatActivity {

    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(btnClickListener);


        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new LanguageFragment()).commit();
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navListener = (@NonNull MenuItem item) -> {

        // TODO NullException
        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.nav_language:
                selectedFragment = new LanguageFragment();
                break;
            case R.id.nav_todolist:
                selectedFragment = new TodoListFragment();
                break;
            case R.id.nav_contact:
                selectedFragment = new ContactFragment();
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                selectedFragment).commit();

        return true;

    };

    private final View.OnClickListener btnClickListener = (View v) -> {

        final int id = v.getId();
        if (id == R.id.btnBack) {
            finish();
        }
    };


}
