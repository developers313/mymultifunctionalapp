package com.projects.mymultifunctionalapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.projects.mymultifunctionalapp.R;

public class MenuActivity extends AppCompatActivity {

    private Button btnBack;
    private Button btnGoogle;
    private Button btnOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        getLayoutObjects();
        setLayoutObjects();

    }

    private void getLayoutObjects() {
        btnBack = findViewById(R.id.btnBack);
        btnGoogle = findViewById(R.id.btnGoogle);
        btnOptions = findViewById(R.id.btnOptions);
    }

    private void setLayoutObjects() {
        btnBack.setOnClickListener(btnClickListener);
        btnGoogle.setOnClickListener(btnClickListener);
        btnOptions.setOnClickListener(btnClickListener);
    }

    private final View.OnClickListener btnClickListener = (View v) -> {

        Intent myIntent;
        final int id = v.getId();
        if (id == R.id.btnBack) {
            finish();
        } else if (id == R.id.btnGoogle) {
            myIntent = new Intent(MenuActivity.this, GoogleActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.btnOptions) {
            myIntent = new Intent(MenuActivity.this, OptionsActivity.class);
            startActivity(myIntent);
        }
    };

}
